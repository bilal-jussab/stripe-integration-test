# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import stripe

from django.db import models

from django.conf import settings
from django_mysql.models import JSONField


# Create your models here.

class Payment(models.Model):
    CHARGE_STATUS_OPTIONS = (
        ("succeeded", "Succeeded"),
        ("failed", "Failed"),
        ("pending", "Pending"),
    )
    name = models.CharField(blank=True, null=True, max_length=250)
    billing_token = models.CharField(blank=True, null=True, max_length=250)
    billing_data = JSONField(blank=True, null=True, default=dict)
    customer_data = JSONField(blank=True, null=True, default=dict)
    customer_id = models.CharField(blank=True, null=True, max_length=250)
    charge_data = JSONField(blank=True, null=True, default=dict)
    status = models.CharField(max_length=25, choices=CHARGE_STATUS_OPTIONS, blank=True, null=True)

    def get_stripe_key(secret=False, publishable=False):
        if secret:
            return settings.STRIPE_KEYS[secret]
        elif publishable:
            return settings.STRIPE_KEYS[publishable]
