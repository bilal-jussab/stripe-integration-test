# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import stripe

from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, CreateView, TemplateView
from django.urls import reverse

from .models import Payment
from .forms import PaymentForm

# Create your views here.

class PaymentListView(ListView):
    model = Payment
    template_name = 'payment/payment_list.html'

    def get_queryset(self):
        return Payment.objects.all()

class PaymentCreateView(CreateView):
    model = Payment
    form_class = PaymentForm
    template_name = 'payment/payment_create.html'

    def get_success_url(self):
        return reverse('payment:payment_list')

    def get_context_data(self, **kwargs):
        context = super(PaymentCreateView, self).get_context_data(**kwargs)
        context['stripe_publishable_key'] = settings.STRIPE_DEMO_PUBLISHABLE_KEY
        return context

    def post(self, request, *args, **kwargs):
        stripe_billing_data = request.POST

        email = stripe_billing_data.get('email')
        paymentmethod_id = stripe_billing_data.get('stripeToken')

        stripe.api_key = settings.STRIPE_DEMO_SECRET_KEY

        # Create a Customer
        try:
            customer = stripe.Customer.create(
                email=email,
                metadata={
                    'backgroundcheck': "BC-1234556",
                    'datacheck': "CH-123456",
                    'check_type': "Check Name",
                    'customer': "Bilal Inc",
                },
            )
            customer_id = customer.get('id')
        except stripe.CardError as e:
            messages.error(self.request, e.message)
            return HttpResponseRedirect(self.get_failure_url())
        except stripe.error.StripeError as e:
            messages.error(self.request, e.message)
            return HttpResponseRedirect(self.get_failure_url())

        payment_method = stripe.PaymentMethod.attach(
            paymentmethod_id,
            customer=customer_id
        )
        customer_data = stripe.Customer.modify(
            customer_id,
            invoice_settings={
                'default_payment_method': paymentmethod_id
            }
        )
        customer_data = stripe.Customer.retrieve(customer_id)

        Payment.objects.get_or_create(
            billing_token=paymentmethod_id,
            billing_data=json.dumps(stripe_billing_data),
            customer_data=json.loads(str(customer_data)),
            customer_id=customer_id,
        )
        return HttpResponseRedirect(self.get_success_url())

class PaymentChargeView(TemplateView):
    template_name = 'payment/payment_charge.html'

    def get_success_url(self):
        return reverse('payment:payment_list')

    def get_object(self):
        return get_object_or_404(
            Payment,
            pk=self.kwargs.get('pk'),
        )

    def get_context_data(self, **kwargs):
        context = super(PaymentChargeView, self).get_context_data(**kwargs)
        context['payment'] = self.get_object()
        return context

    def post(self, request, *args, **kwargs):
        payment = self.get_object()
        stripe.api_key = settings.STRIPE_DEMO_SECRET_KEY

        price_to_charge = 2500

        try:
            charge = stripe.PaymentIntent.create(
                amount=int(round(price_to_charge)),
                currency='gbp',
                payment_method_types=['card'],
                customer=payment.customer_id,
                payment_method=payment.billing_token,
                off_session='one_off',
                confirm=True,
                metadata={
                    'backgroundcheck': "BC-1234556",
                    'datacheck': "CH-123456",
                    'check_type': "Check Name",
                    'customer': "Bilal Inc",
                },
            )
            payment.charge_data = json.loads(str(charge))
            payment.save()
            stripe_customer = stripe.Customer.retrieve(payment.customer_id)
            # stripe_customer.delete()
        # except Exception as e:
            # messages.error(self.request, e.message)
            # return 'error'
        # except stripe.CardError as e:
        #     messages.error(self.request, e.message)
        #     return 'error'
        # except stripe.error.StripeError as e:
        #     messages.error(self.request, e.message)
        #     return 'error'
        # if charge.get('status') == 'succeeded':
        #     return HttpResponseRedirect(self.get_success_url())
