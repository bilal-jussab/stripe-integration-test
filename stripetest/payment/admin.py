# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Payment
# Register your models here.

class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'billing_token',
        'billing_data',
        'customer_data',
        'customer_id',
        'charge_data',
        'status',
    )

admin.site.register(Payment, PaymentAdmin)

