from django.conf.urls import url
from django.contrib import admin

from .views import (
    PaymentListView,
#     CustomerDetailView,
    PaymentCreateView,
    PaymentChargeView,
)

urlpatterns = [
    url(
        r'^$',
        PaymentListView.as_view(),
        name='payment_list'
    ),
    url(
        r'^create/$',
        PaymentCreateView.as_view(),
        name='payment_create'
    ),
    url(
        r'^(?P<pk>[0-9]+)/charge/$',
        PaymentChargeView.as_view(),
        name='payment_charge'
    ),
]
