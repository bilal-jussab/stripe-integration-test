// Custom styling can be passed to options when creating an Element.
var style = {
    base: {
        // Add your base input styles here. For example:
        fontSmoothing: 'antialiased',
        fontSize: '1rem',
        '::placeholder': {
            color: '#a299a6'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
};

// Create an instance of the card elements
var cardNumber = elements.create('cardNumber', {style: style});
cardNumber.mount('#card-number');

var cardExpiry = elements.create('cardExpiry', {style: style});
cardExpiry.mount('#card-expiry');

var cardCvc = elements.create('cardCvc', {style: style});
cardCvc.mount('#card-cvc');

var stripe_elements = [cardNumber, cardExpiry, cardCvc]

var form = document.querySelector('#payment-form');
var error = form.querySelector('#card-errors');
var errorMessage = error.querySelector('.message');

// Listen for errors from each Element, and show error messages in the UI.
var savedErrors = {};
stripe_elements.forEach(function(stripe_element, idx) {
    stripe_element.on('change', function(event) {
        if (event.error) {
            error.classList.remove('hidden-xs-up');
            savedErrors[idx] = event.error.message;
            errorMessage.innerText = event.error.message;
            // add classes for errors
        } else {
            savedErrors[idx] = null;

            // Loop over the saved errors and find the first one, if any.
            var nextError = Object.keys(savedErrors)
                .sort()
                .reduce(function(maybeFoundError, key) {
                    return maybeFoundError || savedErrors[key];
                }, null);

            if (nextError) {
                // Now that they've fixed the current error, show another one.
                errorMessage.innerText = nextError;
            } else {
                // The user fixed the last error; no more errors.
                errorMessage.innerText = '';
                error.classList.add('hidden-xs-up');
            }
        }
    });
});

// get stripe elements field to fit our styling... add classes
// for our sizing/styles and remove its inline ones
var stripe_element = $('.__PrivateStripeElement')
stripe_element.addClass("textinput textInput form-control")
stripe_element.attr('style', '');

var hasSubmitted = false;
// Create a paymentMethod or display an error when the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
    event.preventDefault();

    var billing_details = {
        name: form.querySelector('input#cardholder_name').value,
        line1: form.querySelector('input#cardholder_street').value,
        city: form.querySelector('input#cardholder_city').value, 
        postal_code: form.querySelector('input#cardholder_post_code').value,
        country: 'GB',
    }

    stripe.createPaymentMethod('card', cardNumber).then(function(result) {
        if (result.error) {
            // Inform the customer that there was an error.
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
        } else {
            // Send the paymentMethod to your server.
            if (!hasSubmitted) {
                stripePaymentMethodHandler(result.paymentMethod);
            }
        }
    });
});

function stripePaymentMethodHandler(paymentMethod) {
    // Insert the paymentMethod ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', paymentMethod.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
    hasSubmitted = true;
}
